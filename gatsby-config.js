module.exports = {
    siteMetadata: {
        title: `Erita`,
        description: ``,
        author: `Kacper Drewniak`,
    },
    plugins: [
        `gatsby-plugin-styled-components`,
        `gatsby-plugin-sass`,
        `gatsby-plugin-scroll-reveal`,
        {
            resolve: `gatsby-plugin-intl`,
            options: {
                path: `${__dirname}/src/intl`,
                languages: [`en`],
                defaultLanguage: `en`,
                redirect: true,
            },
        },
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                start_url: `/en`,
                background_color: `black`,
                theme_color: `black`,
                display: `standalone`,
                icon: `src/images/icon.png`
            },
        },
    ],
};
