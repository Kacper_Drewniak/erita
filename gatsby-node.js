exports.onCreatePage = async ({ page, actions }) => {
    if (page.path.match('pl/categories/')) {
        page.matchPath = 'pl/categories/:slug*';
        actions.createPage(page);
    }
    if (page.path.match('en/categories/')) {
        page.matchPath = 'en/categories/:slug*';
        actions.createPage(page);
    }
    if (page.path.match('pl/products/')) {
        page.matchPath = 'pl/products/:slug*';
        actions.createPage(page);
    }
    if (page.path.match('en/products/')) {
        page.matchPath = 'en/products/:slug*';
        actions.createPage(page);
    }
};

