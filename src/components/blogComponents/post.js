import React from "react";
import Block from "../mainComponents/block";
import renderHTML from 'react-render-html';

const Post = ({name, description}) => {
    return <Block
        post
        bgColor="#fee6e9"
        dataSalDisactive
        description={renderHTML(description.html)}
        title={name}
    />
}

export default Post

