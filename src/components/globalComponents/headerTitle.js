import React from "react";

const HeaderTitle = ({title, className, textColor, dataSal}) => <h1 {...dataSal} className={className}
                                                                    style={{color: `${textColor ? textColor : null}`}}>{title}</h1>


export default HeaderTitle;