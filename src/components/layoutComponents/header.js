import React from 'react'
import {headerProperties} from "../../helpers/properties";
import ScrollButton from "./scrollButton";

const Header = () => <header {...headerProperties}>
    <ScrollButton/>
</header>

export default Header;