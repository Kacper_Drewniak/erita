import React,{useState} from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import "../../../style/index.scss";
import Header from "./header";
import Foot from "./foot";
import Navbar from "./navbar";
import MenuIcon from "./MenuIcon";
import {injectIntl, Link} from "gatsby-plugin-intl";

const Layout = ({children, header, className,intl}) => {
    const [open, setOpen] = useState(false);
    const toggle = () => setOpen(!open)

    const {formatMessage} = intl;

    const links = [{
        to: "/",
        label: formatMessage({id: "menu.main"})
    },
        {
            to: "/about",
            label: formatMessage({id: "menu.about"})
        },
        {
            to: "/shop",
            label: formatMessage({id: "menu.shop"})
        },
        {
            to: "/contactForm",
            label: formatMessage({id: "menu.contact"})
        },
        {
            to: "/blog",
            label: formatMessage({id: "menu.blog"})
        }]



    return <><div className=" d-flex justify-content-end position-fixed w-100  menu-icon-wrapper block d-lg-none">
        <MenuIcon
            isOpen={open}
            menuClicked={toggle}
            className={`menu-icon  ${open && "active"}`}
            width={18 * 2}
            height={15 * 2}
            strokeWidth={3}
            rotate={0}
            color='dimgrey'
            borderRadius={0}
            animationDuration={0.5}
        />
    </div>
        <div className={`mobile-menu d-flex flex-column justify-content-center align-items-center d-lg-none  ${open && "active"}`}>
            <a href="/" className="p-4"><img
                className="logo" alt="logo"  height="200px" src="/images/logo.png"/></a>
            {
                links.map(({to, label}) => <Link
                  style={{color:"white",fontSize:"2rem"}}  className="link-item" to={to}>{label}</Link>)
            }
        </div>
        <Navbar/>
        {header && <Header/>}
        <main className={className}>
            {children}
        </main>
        <Foot/>
    </>
}

export default injectIntl(Layout);