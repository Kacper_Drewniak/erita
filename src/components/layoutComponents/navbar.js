import React from "react";
import {brandProperties, linksProperties, logoProperties} from "../../helpers/properties";
import {injectIntl, Link} from "gatsby-plugin-intl"

const Navbar = ({intl}) => {

    const {formatMessage} = intl;

    const links = [{
        to: "/",
        label: formatMessage({id: "menu.main"})
    },
        {
            to: "/about",
            label: formatMessage({id: "menu.about"})
        },
        {
            to: "/shop",
            label: formatMessage({id: "menu.shop"})
        },
        {
            to: "/contactForm",
            label: formatMessage({id: "menu.contact"})
        },
        {
            to: "/blog",
            label: formatMessage({id: "menu.blog"})
        }]



    return <nav id="navbar-top" className="p-0 navbar  d-none  d-lg-flex pl-5 pr-5"><Link {...brandProperties}><img
        {...logoProperties}/></Link>
        <ul {...linksProperties}>
            {
                links.map(({to, label}) => <Link
                    className="link-item" to={to}>{label}</Link>)
            }

        </ul>
    </nav>
}

export default injectIntl(Navbar);