import React from "react";
import {injectIntl, Link} from "gatsby-plugin-intl";

const NavbarFooter = ({intl}) => {

    const {formatMessage} = intl;

    const links = [{
        to: "/",
        label: formatMessage({id: "menu.main"})
    },
        {
            to: "/about",
            label: formatMessage({id: "menu.about"})
        },
        {
            to: "/shop",
            label: formatMessage({id: "menu.shop"})
        },
        {
            to: "/contactForm",
            label: formatMessage({id: "menu.contact"})
        },
        {
            to: "/blog",
            label: formatMessage({id: "menu.blog"})
        }]



    return <nav id="navbar-bottom" className="navbar dark justify-content-between m-0 flex-lg-row pl-lg-5 pr-lg-5 pl-0 pr-0 d-flex flex-column align-items-center">
        <a href="/" className="p-2"><img
            className="logo" alt="logo" height="100px" src="/images/logoGrey.png"/></a>
        <ul className="nav-links p-0 m-0 d-flex flex-column flex-md-row align-items-center" >
            {
                links.map(({to, label}) => <Link className="link-item" to={to}>{label}</Link>)
            }

        </ul>
        <ul className="nav-links m-0 p-0">
            <a href=""><img className="fb-icon m-1" src="/images/facebook.png" alt={""}/></a>
            <a href=""><img className="insta-icon m-1" src="/images/instagram.png" alt={""}/></a>
        </ul>


    </nav>
}

export default injectIntl(NavbarFooter);