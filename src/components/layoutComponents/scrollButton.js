import React from "react";
import scrollTo from 'gatsby-plugin-smoothscroll';
import {injectIntl} from "gatsby-plugin-intl"

const ScrollButton = ({intl}) => {
    const {formatMessage} = intl;
    return <section id="scroll-btn-section" className="demo">
        <a onClick={() => scrollTo('main')}><span/><span/><span/>{formatMessage({id: "buttons.more"})}</a>
    </section>

}

export default injectIntl(ScrollButton);