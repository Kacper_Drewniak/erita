import React from "react";
import {Col, Container, Row} from 'reactstrap'
import HeaderTitle from "../globalComponents/headerTitle";
import {Link} from "gatsby-plugin-intl"

const Block = ({
                   udemmy,
                   title,
                   headerTitleClass,
                   description,
                   img,
                   reverse,
                   bgColor,
                   buttonText,
                   buttonLink
               }) => <Container fluid
                                className="p-0">
    <Row className={`p-0 pt-0 pb-0 pt-md-5 pb-md-5 m-0 ${reverse ? "flex-row-reverse" : ""}`}>
        {img && <Col md="12" lg="5" className="p-0 ">
            <img className="w-100 h-100"  style={{objectFit:"contain"}} alt={title} src={img}/>
        </Col>}
        <Col md="12" lg={img ? "6" : "12"} className={`d-flex flex-column justify-content-around m-0  p-sm-5 p-3 ${reverse ? "align-items-end" : null}`}
             style={{background: bgColor, color: "dimgray"}}>
            <HeaderTitle className={headerTitleClass} title={title}/>
            <p className={reverse ? "text-right" : "text-left"} style={{lineHeight: "250%"}}>{description}</p>
            {buttonLink && buttonText && <>{udemmy ?
                <a className="btn" href={buttonLink}>{buttonText}</a> :
                <Link className="btn" to={buttonLink}>{buttonText}</Link>
            }</>}
        </Col>

    </Row>
</Container>


export default Block;