import React from "react";
import {Card, CardBody, CardGroup, CardImg, CardSubtitle, Row,CardTitle, Col} from "reactstrap";
import {Link} from "gatsby-plugin-intl";

const CardGroupComponent = ({list, item, buttonText}) => <Row className="d-flex justify-content-start pt-0 pb-0 p-0 pb-md-5 pt-md-5">
    {list.map(({name, slug, image, description}) =>
        <Col xxl="2" sm="12" md="4" lg="3" className="p-md-2 p-lg-2">
            <Card className="h-100" style={{background:"#f8f8f8",border:"2px solid dimgrey"}}>
                <CardImg  className="card-image-res"  center width="100%" style={{objectFit:"cover",objectPosition:"bottom center"}} src={image.url} alt={name}/>
                <CardBody className="d-flex flex-column justify-content-between">
                    <div style={{color:"dimgray"}}>
                        <CardTitle tag="h5">{name}</CardTitle>
                        <CardSubtitle tag="h6"
                                      className="pt-2 pb-2 text-muted">{description}</CardSubtitle>
                    </div>
                    <Link className="btn w-100" to={`/${item}/${slug}`}>{buttonText}</Link>
                </CardBody>
            </Card></Col>)}
</Row>


export default CardGroupComponent;