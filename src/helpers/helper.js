export async function fetchCategories() {
    const res = await fetch('https://api-eu-central-1.graphcms.com/v2/ckl2mg5f8u44h01z80wplf9pt/master', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            query: `
                  query {
                  categories {
                    description
                    name
                    slug
                    image {
                      url
                    }
                  }
                }`
        }),
    })
    return await res.json();
}

export async function fetchCategory(slug) {
    const res = await fetch('https://api-eu-central-1.graphcms.com/v2/ckl2mg5f8u44h01z80wplf9pt/master', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            query: `
                  query{
                  category(where: {slug: "${slug}"}) {
                    courses {
                      name
                      slug
                      image{
                        url
                      }
                    }
                  }
                }
                `
        }),
    })
    return await res.json();
}

export async function fetchCourse(slug) {
    const res = await fetch('https://api-eu-central-1.graphcms.com/v2/ckl2mg5f8u44h01z80wplf9pt/master', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            query: `
               query {
               course(where: {slug : "${slug}"}) {
               name
               buttonText
               buttonLink
               description{
                 html
               }
               price
               image{
                 url
               }
              }
            }
                `
        }),
    })
    return await res.json();
}

export async function fetchArticles(){
    const res = await fetch('https://api-eu-central-1.graphcms.com/v2/ckl2mg5f8u44h01z80wplf9pt/master', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
            query: `
            query MyQuery {
              posts {
                slug
                name
                description {
                  html
                }
              }
            }

       `
        }),
    })
    return await res.json();
}

