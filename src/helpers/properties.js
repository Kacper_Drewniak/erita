import React from "react";

export const headerProperties = {
    className: "header",
    ["data-sal"]: "fade",
    ["data-sal-delay"]: 2000,
    ["data-sal-duration"]: 2000,
}

export const logoProperties = {
    className:"logo",
    alt:"logo",
    src:"/images/logo.png"
}

export const brandProperties = {
    to:"/",
    className:"mr-auto",
    ["data-sal"]: "slide-right",
    ["data-sal-delay"]: 500,
    ["data-sal-duration"]: 3600,
}

export const linksProperties = {
    className: "nav-links m-0",
    ["data-sal"]: "slide-left",
    ["data-sal-delay"]: 500,
    ["data-sal-duration"]: 3600,
}


export const layoutProperties = {
    className: "min-vh-100 d-flex flex-column justify-content-center align-items-center"
}

export const mainClass = "bg-white min-vh-100 pl-lg-0 pr-lg-0 pl-xl-5 pr-xl-5 p-0"