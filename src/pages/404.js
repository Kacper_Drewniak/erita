import React from "react"

import Layout from "../components/layoutComponents/layout"

const NotFoundPage = () => (
  <Layout>
404
  </Layout>
)

export default NotFoundPage
