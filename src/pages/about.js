import React from 'react'
import Layout from "../components/layoutComponents/layout";
import {layoutProperties, mainClass} from "../helpers/properties";
import Block from "../components/mainComponents/block";
import {injectIntl, Link} from "gatsby-plugin-intl"

const About = ({intl}) => {

    const {formatMessage} = intl;

    return <Layout {...layoutProperties} className={mainClass}>
        <Block
            img="/images/about.jpg"
            title={formatMessage({id: "aboutEwelina.title"})}
            bgColor="#f8f8f8"
            description={formatMessage({id: "aboutEwelina.text"})}
        />
        <Block
            reverse
            img="/images/logoBlack.png"
            title={formatMessage({id: "about.title"})}
            bgColor="#f8f8f8"
            description={formatMessage({id: "about.text"})}
        />
    </Layout>

}

export default injectIntl(About);

