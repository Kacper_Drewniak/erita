import React, {useEffect, useState} from "react";
import Layout from "../components/layoutComponents/layout";
import {fetchArticles} from "../helpers/helper";
import Post from "../components/blogComponents/post";
import {mainClass} from "../helpers/properties";
import Block from "../components/mainComponents/block";
import renderHTML from "react-render-html";

const Blog = () => {

    const [posts, setPosts] = useState([]);

    useEffect(() => {
        fetchArticles().then(({data}) => {
            setPosts(data.posts)
        })
    })

    return <Layout className={mainClass}>
        <div className="text-center pt-5 ">
            {
                posts.map(({name, description}) => <Block
                    title={name}
                    description={renderHTML(description.html)}
                    bgColor="#fee6e9"
                />)
            }
        </div>
    </Layout>
}

export default Blog;