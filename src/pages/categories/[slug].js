import React, {useEffect, useState} from "react";
import {fetchCategory} from "../../helpers/helper";
import Layout from "../../components/layoutComponents/layout";
import CardGroupComponent from "../../components/shopComponents/cardGroupComponent";
import {mainClass} from "../../helpers/properties";


export default ({params}) => {
    const [courses, setCourses] = useState([])
    useEffect(() => {
        fetchCategory(params.["slug*"]).then(({data}) => setCourses(data.category.courses));
    }, []);

    return <Layout className={mainClass}>
        <CardGroupComponent list={courses} item="products" buttonText="Review"/>
    </Layout>
}
