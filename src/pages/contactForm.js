import React from "react";
import Layout from "../components/layoutComponents/layout";
import {layoutProperties, mainClass} from "../helpers/properties";
import Block from "../components/mainComponents/block";
import {injectIntl} from "gatsby-plugin-intl";

const ContactForm = ({intl}) => {


    const {formatMessage} = intl;

    return <Layout className={mainClass} {...layoutProperties}>
        <Block
            img="/images/1.jpg"
            bgColor="#f8f8f8"
            description={<div className="text-center">
                <h1>{formatMessage({id: "contact.content"})}</h1>
                <h1>Erita Ewa Beńko </h1>
                <h1>43-340 Kozy, ul. Wapienna 2</h1>
                <h1>Regon 240948368</h1>
                <h1>NIP: 9372714172</h1></div>}
            post
        />
    </Layout>
}


export default injectIntl(ContactForm);
