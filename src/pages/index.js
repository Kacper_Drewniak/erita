import React, {useEffect, useState} from 'react';
import Layout from "../components/layoutComponents/layout";
import HeaderTitle from "../components/globalComponents/headerTitle";
import "./../../style/main.scss"
import {fetchCategories} from "../helpers/helper";
import Block from "../components/mainComponents/block"
import {mainClass} from "../helpers/properties";
import {useIntl} from "gatsby-plugin-intl"
import SEO from "../components/layoutComponents/seo";
const IndexPage = () => {
    const [categories, setCategories] = useState([]);

    const {formatMessage} = useIntl();

    const headerTitleProperties = {
        title: formatMessage({id: "headers.whoAreWe"}),
        className: "p-4 text-center",
        textColor: "dimgrey"
    }

    useEffect(() => {
        fetchCategories().then(({data}) => {
            setCategories(data.categories)
        });
    }, []);


    return <Layout header className={mainClass}>
        <SEO title="Home"/>
        <HeaderTitle {...headerTitleProperties}/>
        {categories.map(({name, description, slug, image}, index) => <Block
            bgColor={index % 2 === 0 ? "#fee6e9" : "#f8f8f8"}
            reverse={index % 2 === 0}
            img="/images/1.jpg"
            title={name}
            description={description}
            buttonText="Show Categories"
            buttonLink={`/categories/${slug}`}
        />)}
    </Layout>

}


export default IndexPage
