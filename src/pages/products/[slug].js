import React, {useEffect, useState} from "react";

import {fetchCourse} from "../../helpers/helper";
import Layout from "../../components/layoutComponents/layout";
import Block from "../../components/mainComponents/block";
import renderHTML from 'react-render-html';
import {layoutProperties, mainClass} from "../../helpers/properties";

export default ({params}) => {
    const [course, setCourse] = useState(null)


    useEffect(() => {
        fetchCourse(params.["slug*"]).then(({data}) => {
        setCourse(data.course)
    })}, []);


    return <Layout {...layoutProperties} className={mainClass}>
        {course && <Block
            buttonText="Kup kurs"
            dataSalDisactive
            title={course.name}
            description={renderHTML(course.description.html)}
            img={course.image.url}
            buttonText={course.buttonText}
            buttonLink={course.buttonLink}
            udemmy
            bgColor="#f8f8f8"
        />}
    </Layout>
}