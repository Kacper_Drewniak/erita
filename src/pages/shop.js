import React, {useEffect, useState} from "react";
import Layout from "../components/layoutComponents/layout";
import {fetchCategories} from "../helpers/helper";
import CardGroupComponent from '../components/shopComponents/cardGroupComponent'
import {mainClass} from "../helpers/properties";


const Shop = () => {
    const [categories, setCategories] = useState([]);

    useEffect(() => {
        fetchCategories().then(({data}) => setCategories(data.categories));
    }, []);

    return <Layout className={mainClass}>
        <CardGroupComponent list={categories} item="categories" buttonText="See more"/>
    </Layout>
}

export default Shop;